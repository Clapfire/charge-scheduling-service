# Based on Rebecca Adam's work.

import numpy as np
import pandas as pd
import cvxpy as cp
from matplotlib import pyplot as plt, cm
import sqlite3
import math
import argparse
import json
from pathlib import Path


class Database():
    def __init__(self, db_path):
        self.db = sqlite3.connect(
            db_path,
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        self.db.row_factory = sqlite3.Row

    def __exit__(self):
        self.db.close()

    def get_all_vehicles(self):
        vehicles_db = self.db.execute("SELECT * FROM vehicles").fetchall()

        vehicles = []
        for vehicle in vehicles_db:
            vehicles.append(
                Vehicle(
                    vehicle["id"],
                    vehicle["nickname"],
                    vehicle["manufacturer"],
                    vehicle["model"],
                    vehicle["toa"],
                    vehicle["tod"],
                    vehicle["soc"],
                    vehicle["soc_req"],
                    vehicle["min_chr_rt"],
                    vehicle["max_chr_rt"],
                    vehicle["battery_cap"])
            )

        return vehicles

    def save_schedules(self, vehicles):
        for vehicle in vehicles:
            self.db.execute("UPDATE vehicles SET scheduled = ?, schedule = ?, greedy_schedule = ? WHERE id = ?",
                            (vehicle.scheduled, vehicle.schedule, vehicle.greedy_schedule, vehicle.id))
            self.db.commit()

    def save_load_profile(self, data_path, date):
        df = pd.read_csv(data_path, header=0, parse_dates=[0], index_col=0)

        base_load = df.loc[date]["average"] * 100
        max_load = max(base_load)

        target_load = np.array(((max_load - base_load)/2)
                               ).reshape(len(base_load), 1)

        base_load_json = json.dumps(dict(zip([0.25 * x for x in range(len(base_load.values))],np.round(base_load.values, 2))))
        target_load_json = json.dumps(dict(zip([0.25 * x for x in range(len(target_load))],np.round(target_load[:,0], 2).tolist())))


        try:
            self.db.execute("INSERT INTO grid (date, base_load_profile, target_load_profile) VALUES (?, ?, ?)",(date, base_load_json, target_load_json)
            )

            self.db.commit()
        except self.db.IntegrityError:
            self.db.execute("UPDATE grid SET base_load_profile=?, target_load_profile=? WHERE date=?", (base_load_json, target_load_json, date))
            self.db.commit()

class Vehicle():
    def __init__(self, id, nickname, manufacturer, model, toa, tod, soc, soc_req, min_chr_rt, max_chr_rt, bat_cap):
        self.id = id
        self.nickname = nickname
        self.manufacturer = manufacturer
        self.model = model
        toa_h, toa_m = toa.split(":")
        # self.toa = (4 * int(toa_h)) + np.floor_divide(int(toa_m), 15)
        self.toa = float(toa_h) + float(toa_m)/60.0
        tod_h, tod_m = tod.split(":")
        # self.tod = 4 * int(tod_h) + np.floor_divide(int(tod_m), 15)
        self.tod = float(tod_h) + float(tod_m)/60.0
        self.soc = soc
        self.soc_req = soc_req
        self.min_chr_rt = min_chr_rt
        self.max_chr_rt = max_chr_rt
        self.bat_cap = bat_cap
        self.scheduled = False
        self.schedule = None
        self.greedy_schedule = None

    def __repr__(self):
        return F"""Vehicle
        id:           {self.id}
        nick:         {self.nickname}
        manufacturer: {self.manufacturer}
        model:        {self.model}
        toa:          {self.toa}
        tod:          {self.tod}
        soc:          {self.soc}
        soc_req:      {self.soc_req}
        min_chr_rt:   {self.min_chr_rt}
        max_chr_rt:   {self.max_chr_rt}
        battery_cap:  {self.bat_cap}
        scheduled:    {self.scheduled}
        """


class Scheduler():
    def __init__(self, vehicles, Ts, dataPath, figPath):
        self.figPath = figPath
        self.vehicles = vehicles
        self.N = len(self.vehicles)  # Number of vehicles
        self.Ts = Ts  # Timestep
        self.J = int(1/self.Ts)  # Number of time slots per hour
        self.K = 24*self.J  # Number of time slots 24 hours

        # Battery capacity of each vehicles
        self.bcap = np.asarray([x.bat_cap for x in self.vehicles])
        # State of charge for each vehicle
        self.soc = np.asarray([x.soc for x in self.vehicles])
        # Requested state of charge for each vehicle
        self.soc_req = np.asarray([x.soc_req for x in vehicles])
        # Maximum power for charging station
        self.pmax = 50*np.ones((self.K, 1))
        # Maximum charge rate for each vehicle
        self.vmax = np.asarray([x.max_chr_rt * np.ones((self.K, 1))
                               for x in vehicles]).flatten()
        # Minimum charge rate for each vehicle
        self.vmin = np.asarray([x.min_chr_rt * np.ones((self.K, 1))
                               for x in vehicles]).flatten()
        # Time of arrival for each vehicle
        self.toa = np.asarray([x.toa for x in self.vehicles])
        # Time of departure for each vehicle
        self.tod = np.asarray([x.tod for x in self.vehicles])

        # State of charge when fully charged
        self.q = np.zeros((self.N, 1))
        for i in range(self.N):
            self.q[i] = (1/self.Ts) * (1 - self.soc[i]/100) * self.bcap[i]

        # Minimum state of charge at end of charging is as requested
        self.q_min = np.zeros((self.N, 1))
        for i in range(self.N):
            self.q_min[i] = (1/self.Ts) * (1 - (self.soc_req[i] - self.soc[i])) * self.bcap[i]

        self.C = self.make_connection_matrix(
            self.toa, self.tod, self.N, self.K, self.J)

        # Target function
        self.target = self.get_load_data(dataPath)
        self.cfit = np.sum(self.C, axis=0)
        self.target[(self.cfit == 0.0)] = 0

        # Gradient
        self.c = self.C.flatten()
        self.A = np.dot(np.kron(np.ones((1, self.N)),
                        np.eye(self.K)), np.diagflat(self.c, k=0))

        self.grad = -2 * np.dot(np.transpose(self.target), self.A)
        self.H = 2 * np.dot(np.transpose(self.A), self.A)

        # self.result = self.solve_mixed_integer()
        self.result = self.solve_quadratic()

        # Dump results for each vehicle to json object
        for i, vehicle in enumerate(self.vehicles):
            vehicle.scheduled = True
            vehicle.schedule = json.dumps(dict(zip(
                [x * 0.25 for x in range(self.K)],
                np.round(self.result[i, :], 2).tolist()
            )))

        self.calculate_unscheduled()
        # Create graph
        self.graph()

    # Make connection matrix
    def make_connection_matrix(self, toa, tod, N, K, J):
        """
        Makes the connection matrix described in document

            Parameters:
                toa(array): List of time of arrival for vehicles
                tod(array): List of time of departure for vehicles
                N(int):    Number of vehicles
                K(int):    Number of time slots for 24 hours
                J(int):    Number of time slots for 1 hour

            Returns:
                C(array): Connection matrix
        """

        C = np.zeros((N, K))

        for cntn in range(N):
            for cntk in range(K):
                if(toa[cntn] <= cntk/J and tod[cntn] >= cntk/J):
                    C[cntn, cntk] = 1

        return C

    # Get load data
    def get_load_data(self, data_path):
        df = pd.read_csv(data_path, header=0, parse_dates=[0], index_col=0)

        base_load = df.loc["2017-08-08"]["average"] * 100
        max_load = max(base_load)

        target_load = np.array(((max_load - base_load)/2)
                               ).reshape(len(base_load), 1)

        return target_load
    
    def solve_quadratic(self):
        N = self.N
        K = self.K
        vmax = self.vmax
        c = self.c
        A = self.A
        target = self.target
        pmax = self.pmax
        q = self.q
        q_min = self.q_min
        grad = self.grad
        H = self.H

        G1 = -1 * np.eye(N * K)
        h1 = np.zeros((N * K, 1))
        h1 = h1.reshape(h1.shape[0], 1)

        G2 = np.eye(N * K)
        h2 = np.dot(np.diagflat(c, k=0), vmax)
        h2 = h2.reshape(h2.shape[0], 1)

        G3 = A
        h3 = np.minimum(target, pmax)

        B = np.dot(np.kron(np.eye(N), np.ones((1, K))), np.diagflat(c, k=0))

        G4 = B
        h4 = q

        G5 = -1 * B
        h5 = -1 * q_min

        G = np.vstack((np.vstack((np.vstack((np.vstack((G1,G2)),G3)),G4)),G5))
        # print(h1.shape)
        # print(h2.shape)
        # h_12 = np.vstack((h1, h2))
        # h_123 = np.vstack((h_12, h3))
        # h_1234 = np.vstack((h_123, h4))
        # h_12345 = np.vstack((h_1234, h5))
        # h = h_12345.flatten()
        h = np.vstack((np.vstack((np.vstack((np.vstack((h1,h2)),h3)),h4)),h5)).flatten()

        x = cp.Variable(N * K)
        prob = cp.Problem(cp.Minimize((1/2) * cp.quad_form(x, H) + grad @ x), [G @ x <= h])

        print(prob.solve())

        result = np.zeros((self.N, self.K))
        for n in range(self.N):
            for k in range(self.K):
                result[n, k] = x.value[n*self.K + k]

        return result

    def solve_mixed_integer(self):

        # Constraint Matrix
        # Min Charge Power
        G11_1 = np.diagflat(np.dot(np.diagflat(self.c, k=0), self.vmin), k=0)
        G12_1 = -1 * np.eye(self.N * self.K)

        # Max Charge Power
        G11_2 = -1 * \
            np.diagflat(np.dot(np.diagflat(self.c, k=0), self.vmax), k=0)
        G12_2 = np.eye(self.N * self.K)

        G11 = np.vstack((G11_1, G11_2))
        G12 = np.vstack((G12_1, G12_2))
        h1 = np.vstack((np.zeros((self.N * self.K, 1)),
                       np.zeros((self.N * self.K, 1))))

        # Max Charge in total
        G22_1 = self.A
        # Target and max charge limits are constraints
        h2_1 = np.minimum(self.target, self.pmax)

        # Battery level
        B = np.dot(np.kron(np.eye(self.N), np.ones(
            (1, self.K))), np.diagflat(self.c, k=0))

        G22_2 = B
        h2_2 = self.q
        G22_3 = -1 * B
        h2_3 = -1 * self.q_min
        G22 = np.vstack((np.vstack((G22_1, G22_2)), G22_3))
        h2 = np.vstack((np.vstack((h2_1, h2_2)), h2_3))
        G21 = np.zeros((G22.shape[0], G11.shape[1]))

        G = np.vstack((np.hstack((G11, G12)), np.hstack((G21, G22))))
        h = np.vstack((h1, h2)).flatten()

        # Binary slack variable x, (y >= vmin * x, y <= vmax * x)
        x = cp.Variable(self.N * self.K, boolean=True)
        y = cp.Variable(self.N * self.K)

        prob = cp.Problem(cp.Minimize((1/2) * cp.quad_form(y, self.H) +
                          self.grad @ y), [G @ cp.hstack((x, y)) <= h])  # ,A @ x == b

        print(prob.solve('SCIP', verbose=True))

        result = np.zeros((self.N, self.K))
        for n in range(self.N):
            for k in range(self.K):
                result[n, k] = y.value[n*self.K + k]

        return result

    def calculate_unscheduled(self):
        for vehicle in self.vehicles:
            schedule = np.zeros(self.K)

            energy_left = vehicle.bat_cap * ((vehicle.soc_req - vehicle.soc)/100)
            t = int(vehicle.toa * self.J)

            while energy_left > 0:
                if energy_left - vehicle.max_chr_rt >= 0:
                    energy_left -= vehicle.max_chr_rt
                    schedule[t:t+4] = vehicle.max_chr_rt
                    t += self.J
                else:
                    schedule[t] = energy_left
                    energy_left = 0

            vehicle.greedy_schedule = json.dumps(dict(zip(
                [x * 0.25 for x in range(self.K)],
                np.round(schedule, 2).tolist()
            )))

    def graph(self):
        # ---- plot stacked powers ----
        plt.figure()
        x_axis = np.arange(1, self.K+1, 1)/self.J - 1/self.J/2
        plt.plot(x_axis, self.pmax, color='black', linewidth=1)
        plt.plot(x_axis, self.target, color='green', linewidth=1)
        pltcolors = cm.rainbow(np.linspace(0, 1, self.N))
        barWidth = 1/self.J
        plt.bar(x_axis, self.result[0, :], color=pltcolors[0], width=barWidth)
        bars_ud_all = np.zeros((self.K, 1))
        for i in range(1, self.N):
            bars_ud = self.result[i-1, :]
            if i > 1:
                bars_ud = np.add(bars_ud, bars_ud_all)
            bars_ud_all = bars_ud
            plt.bar(x_axis, self.result[i, :], bottom=bars_ud_all.tolist(
            ), color=pltcolors[i], width=barWidth)

        x_low = math.floor(np.min([x.toa for x in self.vehicles]))
        x_up = math.ceil(np.max([x.tod for x in self.vehicles]))+1
        plt.xlim([x_low, x_up])
        plt.xticks(np.arange(x_low, x_up, 1))

        labeltxt = np.hstack(['Grid Load', 'Target Load'])
        for i in range(self.N):
            labeltxt = np.hstack([labeltxt, self.vehicles[i].nickname])
        plt.legend(labels=labeltxt)
        plt.xlabel("Time")
        plt.ylabel("Power")
        plt.savefig(self.figPath, dpi=1200)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Calculate schedules for EV charging based on vehicles in specified database")
    parser.add_argument("db_path", metavar="Database_Path", type=Path)
    parser.add_argument("data_path", metavar="Load_Data_Path", type=Path)
    parser.add_argument("fig_path", metavar="Figure_Path", type=Path)
    parser.add_argument("Ts", metavar="Timestep", type=float)

    args = parser.parse_args()

    db = Database(args.db_path)

    vehicles = db.get_all_vehicles()

    scheduler = Scheduler(vehicles, args.Ts, args.data_path, args.fig_path)

    db.save_load_profile(args.data_path, "2017-08-08")

    db.save_schedules(scheduler.vehicles)
