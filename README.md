# Charge Scheduling Service

## Web Portal

A web portal for collecting necessary information for the scheduling service. Stores data in a database, which can be accessed by the scheduling service.

## Scheduling Service

Queries necessary information from the database and calculates a schedule. The output is a JSON schedule based on the OCPP standard.

## Distribution Service

Communicates schedules with appropiate charging stations or vehicles.
