from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, current_app
)
from importlib.metadata import re

from webapp.auth import login_required
from webapp.db import get_db

import subprocess
import json

bp = Blueprint("vehicles", __name__)


@bp.route("/overview", methods=("GET", "POST"))
def overview():
    db = get_db()

    initial = request.args.get("initial", False)

    vehicles = db.execute(
        "SELECT * FROM vehicles WHERE scheduled = 1").fetchall()

    vehicles_unscheduled = db.execute(
        "SELECT * FROM vehicles WHERE scheduled = 0").fetchall()
    
    # Run optimization if requested
    if request.method == "POST" or initial or len(vehicles) == 0:
        completedProcess = subprocess.run(
            ["python /code/scheduler/optimize.py /code/instance/flaskr.sqlite /code/scheduler/example_load.csv /code/webapp/static/images/graph.png 0.25"], shell=True)
        if completedProcess.returncode != 0:
            flash("Error calculating schedule, likely that no feasible solution was found")
        else:
            return redirect(url_for("vehicles.overview"))

    if len(vehicles) + len(vehicles_unscheduled) < 1:
        return redirect(url_for("vehicles.register"))

    grid = db.execute("SELECT * FROM grid WHERE date = '2017-08-08'").fetchone()

    if grid == None:
        return redirect(url_for("vehicles.overview", initial=True))
    load = json.loads(grid["base_load_profile"]).values()
    target = json.loads(grid["target_load_profile"]).values()

    labels = []
    values = dict()
    greedy_values = dict()

    # All labels are the same for each schedule
    labels = json.loads(vehicles[0]["schedule"]).keys()
    if len(vehicles) != 0:
        for vehicle in vehicles:
            values[vehicle["id"]] = json.loads(vehicle["schedule"]).values()
            greedy_values[vehicle["id"]] = json.loads(vehicle["greedy_schedule"]).values()

    
    return render_template("vehicles/overview.html", vehicles=vehicles, vehicles_unscheduled=vehicles_unscheduled, labels=labels, values=values, greedy_values=greedy_values, load=load, target=target)


@bp.route("/register", methods=("GET", "POST"))
@login_required
def register():
    if request.method == "POST":
        db = get_db()

        nickname = request.form["nickname"]
        manufacturer = request.form["manufacturer"]
        model = request.form["model"]
        if request.form["iec61851"] == "on":
            iec61851 = True
        else:
            iec61851 = False
        if request.form["iso15118"] == "on":
            iso15118 = True
        else:
            iso15118 = False

        min_chr_rt = request.form["min_chr_rt"]
        max_chr_rt = request.form["max_chr_rt"]
        battery_cap = request.form["battery_cap"]
        tod = request.form["tod"]
        toa = request.form["toa"]
        soc = request.form["soc"]
        soc_req = request.form["soc_req"]

        error = None

        if not nickname:
            error = "Nickname required."

        if not iec61851:
            error = "IEC 61851 support must be specified"

        if not iso15118:
            error = "ISO 15118 support must be specified"

        if not min_chr_rt:
            error = "Minimum charge rate must be specified"
        else:
            min_chr_rt = int(min_chr_rt)

        if not max_chr_rt:
            error = "Maximum charge rate must be specified"
        else:
            max_chr_rt = int(max_chr_rt)

        if not max_chr_rt:
            error = "Battery capacity must be specified"
        else:
            battery_cap = int(battery_cap)

        if error is None:
            try:
                db.execute(
                    """INSERT INTO vehicles (nickname, userId, manufacturer, model, iec61851, iso15118, tod, toa, soc, soc_req, min_chr_rt, max_chr_rt, battery_cap, scheduled)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                    (nickname, g.user["userId"], manufacturer, model,
                     iec61851, iso15118, tod, toa, soc, soc_req, min_chr_rt, max_chr_rt,
                     battery_cap, False)
                )

                db.commit()
            except db.IntegrityError:
                error = "Nickname already in use, please use another."
            else:
                return redirect(url_for("vehicles.overview"))
        flash(error)

    return render_template("vehicles/register.html")


@bp.route("/update", methods=("GET", "POST"))
@login_required
def update():
    if request.method == "POST":
        db = get_db()
        error = None

        vehicleId = request.form["vehicleId"]
        tod = request.form["tod"]
        toa = request.form["toa"]
        soc = request.form["soc"]
        soc_req = request.form["soc_req"]

        try:
            db.execute("UPDATE vehicles SET tod = ?, toa = ?, soc = ?, soc_req = ? WHERE id = ?",
                       (tod, toa, soc, soc_req, vehicleId))
            db.commit()
        except db.IntegrityError:
            error = "Invalid VehicleId"
        else:
            return redirect(url_for("vehicles.overview"))

        flash(error)

    return render_template("vehicles/update.html", vehicleId=request.args.get("vehicleId"))


@bp.route("/delete", methods=["POST"])
@login_required
def delete():
    db = get_db()
    vehicleId = request.form["vehicleId"]

    cur = db.cursor()
    cur.execute("DELETE FROM vehicles WHERE id = ?", (vehicleId,))
    db.commit()
    flash(F"Succesfully deleted vehicle with ID: {vehicleId}")
    return redirect(url_for("vehicles.overview"))
