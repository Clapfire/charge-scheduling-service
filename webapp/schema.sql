DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS vehicles;
DROP TABLE IF EXISTS grid;

CREATE TABLE users (
    userId INTEGER PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    name TEXT,
    company TEXT
);

CREATE TABLE vehicles (
    id INTEGER PRIMARY KEY,
    nickname TEXT UNIQUE NOT NULL,
    userId INTEGER NOT NULL,
    manufacturer TEXT NOT NULL,
    model TEXT NOT NULL,
    IEC61851 BOOL NOT NULL,
    ISO15118 BOOL NOT NULL,
    toa INTEGER,
    tod INTEGER,
    soc INTEGER,
    soc_req INTEGER,
    min_chr_rt INTEGER NOT NULL,
    max_chr_rt INTEGER NOT NULL,
    battery_cap INTEGER NOT NULL,
    scheduled BOOL NOT NULL,
    schedule JSON,
    greedy_schedule JSON,
    FOREIGN KEY (userId) 
        REFERENCES user(userId)
);

CREATE TABLE grid (
    date dateTime PRIMARY KEY,
    base_load_profile JSON,
    target_load_profile JSON
);