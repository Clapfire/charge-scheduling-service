flask
ipykernel
numpy
pandas
cvxpy==1.2.0
cvxopt
matplotlib
pyscipopt